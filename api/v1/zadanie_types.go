/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// ZadanieSpec defines the desired state of Zadanie
type ZadanieSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file
	// Foo is an example field of Zadanie. Edit zadanie_types.go to remove/update

	//this will contain the full name of author
	AuthorName string `json:"authorName,omitempty"`

	// this will be the OL identifier of author
	AuthorID string `json:"authorID,omitempty"`

	// this will be howmuch works to get
	WorksLimit int32 `json:"worksLimit"`
}

// this defines the state
type ZadanieStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	// this is the total amount of works it got
	CountOfWorks int32 `json:"countOfWorks,omitempty"`

	//this will be list of works
	Works []string `json:"works,omitempty"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// linking stuff to zadanies API
type Zadanie struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   ZadanieSpec   `json:"spec,omitempty"`
	Status ZadanieStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// contains a list of this upper zadanie struct
type ZadanieList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Zadanie `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Zadanie{}, &ZadanieList{})
}
