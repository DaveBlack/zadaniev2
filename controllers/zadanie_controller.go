/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/go-logr/logr"
	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"

	webappdfv1 "gitlab.com/DaveBlack/zadaniev2/api/v1"
)

// reconciles a zadanie object what was defined in types
type ZadanieReconciler struct {
	client.Client
	Log    logr.Logger
	Scheme *runtime.Scheme
}

// googled for some fix
func NewZadanieReconciler(client client.Client, log logr.Logger, scheme *runtime.Scheme) *ZadanieReconciler {
	return &ZadanieReconciler{
		Client: client,
		Log:    log.WithName("controllers").WithName("Zadanie"),
		Scheme: scheme,
	}
}

// the stucture from openlibs website https://openlibrary.org/authors/OL26320A/works.json?limit=50
type OpenLibraryAuthor struct {
	Key   string `json:"key"`
	Name  string `json:"name"`
	Works []struct {
		Key  string `json:"key"`
		Type struct {
			Key string `json:"key"`
		} `json:"type"`
	} `json:"works"`
}

func fetchAuthorWorks(authorName, authorID string, worksLimit int32) ([]string, error) {
	// define openlibbaseurl
	baseURL := "https://openlibrary.org"

	// check for authorName or authorid
	var authorURL string
	if authorName != "" {
		authorURL = baseURL + "/search/authors.json?q=" + authorName
	} else {
		authorURL = baseURL + "/authors/" + authorID + ".json"
	}

	resp, err := http.Get(authorURL)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	// if using authorName, get the author id
	if authorName != "" {
		var authorsResp struct {
			Authors []struct {
				Key string `json:"key"`
			} `json:"docs"`
		}
		err = json.Unmarshal(body, &authorsResp)
		if err != nil {
			return nil, err
		}

		if len(authorsResp.Authors) == 0 {
			return nil, fmt.Errorf("no authors found with name %s",
				authorName)
		}
		authorID = authorsResp.Authors[0].Key[9:] // Remove the "/authors/" prefix from the key
	}
	// get the list of works by author ID
	worksURL := baseURL + "/authors/" + authorID + "/works.json?limit=" + strconv.Itoa(int(worksLimit))
	resp, err = http.Get(worksURL)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	// parse the list of works
	var worksResp struct {
		Entries []struct {
			Title string `json:"title"`
		} `json:"entries"`
	}
	err = json.Unmarshal(body, &worksResp)
	if err != nil {
		return nil, err
	}

	// extract the work ttles
	works := make([]string, len(worksResp.Entries))
	for i, entry := range worksResp.Entries {
		works[i] = entry.Title
	}

	return works, nil
}

//+kubebuilder:rbac:groups=webappdf.df.zadaniev2,resources=zadanies,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=webappdf.df.zadaniev2,resources=zadanies/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=webappdf.df.zadaniev2,resources=zadanies/finalizers,verbs=update

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the Zadanie object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.14.1/pkg/reconcile

func (r *ZadanieReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := r.Log.WithValues("zadanie", req.NamespacedName)

	// get the Zadanie instance
	zadanie := &webappdfv1.Zadanie{}
	err := r.Get(ctx, req.NamespacedName, zadanie)
	if err != nil {
		log.Error(err, "unable to fetch Zadanie")
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}

	// check if authorname or authorid is provided and if the workslimit good
	if zadanie.Spec.AuthorName == "" && zadanie.Spec.AuthorID == "" {
		err := errors.New("authorName or authorID must be provided")
		log.Error(err, "invalid Zadanie spec")
		return ctrl.Result{}, err
	}

	if zadanie.Spec.WorksLimit <= 0 {
		err := errors.New("worksLimit must be greater than 0")
		log.Error(err, "invalid Zadanie spec")
		return ctrl.Result{}, err
	}

	// handle deletion with annotation check
	if !zadanie.ObjectMeta.DeletionTimestamp.IsZero() {
		if zadanie.ObjectMeta.Annotations["delete-confirm"] != "true" {
			log.Info("Zadanie cannot be deleted without delete-confirm annotation set to 'true'")
			return ctrl.Result{}, nil
		}
	}

	// get the list of works and update the Zadanie status
	works, err := fetchAuthorWorks(zadanie.Spec.AuthorName, zadanie.Spec.AuthorID, zadanie.Spec.WorksLimit)
	if err != nil {
		log.Error(err, "failed to fetch author works")
		return ctrl.Result{}, err
	}

	// update status with the fetched works
	zadanie.Status.CountOfWorks = int32(len(works))
	zadanie.Status.Works = works

	if err := r.Status().Update(ctx, zadanie); err != nil {
		log.Error(err, "unable to update Zadanie status")
		return ctrl.Result{}, err
	}

	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the manager.
func (r *ZadanieReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&webappdfv1.Zadanie{}).
		Complete(NewZadanieReconciler(mgr.GetClient(), ctrl.Log, mgr.GetScheme()))
}
